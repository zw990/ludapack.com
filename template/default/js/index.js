$(function () {
    function scrollHeadle() {
        if ($(window).scrollTop() > 60) {
            $(".header").removeClass("home_header")
            $(".header").css('box-shadow','0 2px 3px rgb(0 0 0 / 20%)')
        } else {
            $(".header").addClass("home_header")
                $(".header").css('box-shadow','0 0 0')
        }
    }
    scrollHeadle()
    
    
    $(window).on("scroll", scrollHeadle)

    /* 分页点击 */
    $(".pages .num span").on("click", function () {
        $(this).addClass("active").siblings().removeClass("active")
    })
    // 微信 
    $('.wxclick').mouseover(function(){
       $('.footer main form .span img').eq(0).show()
    })
     $('.wxclick').mouseout(function(){
         $('.footer main form .span img').eq(0).hide()
    })
    
      $('.wbclick').mouseover(function(){
       $('.footer main form .span img').eq(1).show()
    })
     $('.wbclick').mouseout(function(){
         $('.footer main form .span img').eq(1).hide()
    })


    /* wow */
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: true
    });
    wow.init();

   /* 顶部导航菜单相关 */
    function topNavHandle() {
        if ($(window).width() > 1250) {
            $(".header .header_nav li").hover(
                function () {
                    $(this).children(".menu_sxl").stop().fadeIn();
                },
                function () {
                    $(this).children(".menu_sxl").stop().fadeOut();
                }
            );
        } else {
            /* 移动端顶部菜单按钮点击 */
            $(".app_nav_btn").click(function (e) {
                $(".app_nav_btn,.header_nav").toggleClass("check");
                $(".header_nav>li.menu>a").removeClass("kai");
                e.stopPropagation();
            });
            /* 移动端顶部菜单项点击 */
            $(".header_nav>li.menu>a").click(function (e) {
                $(this).toggleClass("kai")
                $(this).next(".menu_sxl").stop().slideToggle()
                $(this).parent().siblings().find(".menu_sxl").slideUp()
                $(this).parent().siblings().children("a").removeClass("kai")
                return false
            })
            /* 移动端顶部子菜单项点击 */
            $(".header_nav>li.menu .menu_sxl a").click(function (e) {
                $(".app_nav_btn,.header_nav").toggleClass("check");
                $(".header_nav>li.menu>a").removeClass("kai");
            })
        }
    }
    topNavHandle()

    /* 内页二级菜单点击 */
    if ($(".su_menu_list .su_menu_item").length > 0) {
        $(".su_menu_list .su_menu_item").on("click", function (e) {
            $(this).addClass("active").siblings().removeClass("active")
            let left = $(this).offset().left + e.currentTarget.clientWidth - 50 + "px"
            $(".main .su_menu .bar span").css({ left })
        })
        let left = $(".su_menu_list .su_menu_item.active").offset().left + $(".su_menu_list .su_menu_item.active").width() + "px"
        $(".main .su_menu .bar span").css({ left })
    }


    /* 锚点 */
    let id = (location.hash && location.hash.split("#")[1]) || "";
    if (id) {
        /* 如果有锚点 跳转到锚点位置并向上偏移 */
        $(window).scrollTop($(window).scrollTop() - $(window).height() / 5)
    } else {
        /* 如果没有锚点 */
        $(window).scrollTop(
            ($(".banner").height() + $(".location").height()) * 0.6
        )
    }
    /* 顶部菜单二级菜单点击 锚点本页面 */
    $(".active.menu .c_scroll,.su_menu_list .su_menu_item").click(function (e) {
        let scrollTop = $(`#${$(this).attr("md")}`).offset().top - $(window).height() / 5;
        $('html , body').animate({ scrollTop }, 'slow');
        return false;
    });
    
    $('#business a').eq(1).addClass('on')
    $('#business a').mouseover(function(){
        $(this).addClass('on').siblings('a').removeClass('on')
    })
})